
public class Volumenberechnung {

	public static double wuerfel (double zahl1) {
	
		return zahl1*zahl1*zahl1;
	}
	
	public static double quader (double zahl1,double zahl2,double zahl3) {
		
		return zahl1*zahl2*zahl3;
	}
	public static double pyramide (double zahl1,double zahl2) {
		
		return zahl1*zahl1*zahl2/3;
	}
	public static double kugel (double zahl1) {
		
		return zahl1*zahl1*zahl1*1.33*3.14;
	}
	public static void main(String[] args) {
	
		double a=10;
		double b=4;
		double c=6;
		double h=2;
		double r=7;
		double erg1;
		double erg2;
		double erg3;
		double erg4;
		
		erg1=wuerfel(a);
		erg2=quader(a,b,c);
		erg3=pyramide(a,h);
		erg4=kugel(r);
		
		System.out.println("W�rfel:"+ erg1);
		System.out.println("Quader:"+ erg2);
		System.out.println("Pyramide:"+erg3);
		System.out.println("Kugel:"+erg4);
	}

}
