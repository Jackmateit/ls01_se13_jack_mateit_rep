import java.util.Scanner;
import java.lang.Math;

public class Schleifen_A4_4 {

	public static void main(String[] args) {
		
	//Aufgabe 1: Z�hlen
		/*
		int n = 1;
		Scanner sc = new Scanner (System.in);
		System.out.print("Bitte geben Sie n ein: ");
		n = sc.nextInt();
		//Heraufz�hlend
		for(int i = 1; i <= n; i++)
		//for(int i = 0; i < 10;i++) {}//int i = 0; definiert und deklariert i // i < 10; bestimmt Bedingung wie lange die For Schleife gehen soll // i++ addiert jede Wiederholung i + 1
		{
			
			System.out.print(i);
			if (i >= n )
			{
				System.out.print(" \n");
			}
			else
			{
				System.out.print(", ");
			}
		
		}
		//Hinunterz�hlend
				
		for(int i = 1; i <= n; n--)
		{
			System.out.print(n);
			if (n <= i)
			{
				System.out.print(" ");
			}
			else
			{
				System.out.print(", ");
			}				
		}*/
	//Aufgabe 8: Quadrat
		/*Scanner sc = new Scanner(System.in);
		System.out.print("Bitte geben Sie die Seitenl�nge ein: ");
		int anzahl = 0;
		anzahl = sc.nextInt();
		for(int i = 0; i < anzahl; i++)
		{
			System.out.println("");
			System.out.printf( "%s" ,"* ");
			for(int u = 1; u < anzahl; u++)
			{
				System.out.printf( "%s" ,"* ");
			}
		}
		*/
	//Aufgabe 2: Fakult�t //0-20 Fakult�t
		/*Scanner sc = new Scanner(System.in);
		int zahl1 = 0;
		int ergebnis = 1;
		boolean loop1 = true;
		while (loop1 == true) {
				System.out.print("Bitte geben Sie eine Zahl von 0 bis 20 ein: ");
				zahl1 = sc.nextInt();
			if((zahl1 >= 0) &&(zahl1 <=20)) {
				loop1 = false;
				break;
			}
			else {
			}
		}
		for(int i = 1; i <= zahl1;i++) {
			ergebnis = ergebnis * i;

		}
		System.out.println(zahl1+"! = "+ergebnis);
		*/
	//Aufgabe 5: Zinseszins
		Scanner sc = new Scanner(System.in);
		double laufzeit=0, kapital=0, zinssatz=0, eingez_kapital=0, ausgez_kapital=0;
		
		System.out.println("Wilkommen bei der OSZ IMT Sparkasse. Wir freuen uns das Sie sie einen Sparvertrag einrichten wollen. Bitte f�llen sie das nachfolgende Formular aus:");
		System.out.print("\nLaufzeit (in Jahren) des Sparvertrags: "); 
		laufzeit = sc.nextDouble();
		System.out.print("\nWie viel Kapital (in Euro) m�chten Sie anlegen: ");
		kapital = sc.nextDouble();
		System.out.print("\nZinssatz: ");
		zinssatz = sc.nextDouble();
		
		//Berechnung
		ausgez_kapital = kapital*powerFunction(zinssatz, laufzeit);
		System.out.print("\nEingezahltes Kapital: "+eingez_kapital);
		System.out.print("\nAusgezahltes Kapital: "+ausgez_kapital);
	}
	static double powerFunction(double base, double exponent) {
	       double result = 1;
	       for (int i = 0; i < exponent; i++) {
	           result = base * result;
	       }
	       return result;
	   }

}
