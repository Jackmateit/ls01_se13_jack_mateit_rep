
public class Konsolenausgabe {

	public static void main(String[] args) {
	
		//Aufgabe 1
		System.out.print("Das ist ein Beispielsatz. Ein Beispielsatz ist das.");
		
		
		//Aufgabe 2 
		
		System.out.printf("\n%20s","*");
		System.out.printf("\n%21s","***");
		System.out.printf("\n%22s","*****");
		System.out.printf("\n%23s","*******");
		System.out.printf("\n%24s","*********");
		System.out.printf("\n%25s","***********");
		System.out.printf("\n%26s","*************");
		System.out.printf("\n%21s","**");
		System.out.printf("\n%21s\n","**");
		
		
		//Aufgabe 3
		
		double a =22.4234234;
		double b =111.2222;
		double c =4.0;
		double d =1000000.551;
		double e =97.34;
		
		
		System.out.printf( "%.2f\n" , a);
		System.out.printf( "%.2f\n" , b);
		System.out.printf( "%.2f\n" , c);
		System.out.printf( "%.2f\n" , d);
		System.out.printf( "%.2f\n" , e);
	}

}
