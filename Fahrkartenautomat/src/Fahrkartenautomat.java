﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag = 0.0;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       // Geldeinwurf
       eingezahlterGesamtbetrag = geldeinwurf(zuZahlenderBetrag);
       // Fahrscheinausgabe
       fahrscheinausgabe();
       // Rückgeldberechnung und -Ausgabe
       rueckgeldberechnung(zuZahlenderBetrag, eingezahlterGesamtbetrag);
    }
       public static double fahrkartenbestellungErfassen () {

    	   	Scanner tastatur = new Scanner(System.in);
        	double zuZahlenderBetrag = 0; 
            double anzahlDerTickets = 0;
            boolean loop1 = true;
            boolean loop2 = true; 
            double ticketpreis=0;
        	
            double [] doublearray = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
            
            String [] fahrkartenarray = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
            
            while(loop1 == true) {
            System.out.print("Fahrkartenbestellvorgang:" +"\n========================================="+"\nEinzelfahrschein Berlin AB (1)"+"\nEinzelfahrschein Berlin BC (2)"+"\nEinzelfahrschein Berlin ABC (3)"+"\nKurzstrecke (4)"+"\nTageskarte Berlin AB (5)"+"\nTageskarte Berlin BC (6)"+"\nTageskarte Berlin ABC (7)"+"\nKleingruppen-Tageskarte Berlin AB (8)"+"\nKleingruppen-Tageskarte Berlin BC (9)"+"\nKleingruppen-Tageskarte Berlin ABC (10)\n========================================="+"\nIhre Wahl: "); 	
            int welchesTicketIn = tastatur.nextInt();
            
            switch(welchesTicketIn)
     		  {
     		    case 1:
     		        System.out.println("Sie haben einen "+ fahrkartenarray [0] +" gewählt.");
     		        ticketpreis = doublearray [0] ;
     		        loop1 = false;
     		      break;
     		    case 2:
     		    	System.out.println("Sie haben einen "+ fahrkartenarray [1] +" gewählt.");
     		    	ticketpreis = doublearray [1];
     		    	loop1 = false;
     		      break;
     		    case 3:
     		    	System.out.println("Sie haben eine "+ fahrkartenarray [2] +" gewählt.");
     		    	ticketpreis = doublearray [2];
     		    	loop1 = false;
     			  break;
     			case 4:
     				System.out.println("Sie haben eine "+ fahrkartenarray [3] +" gewählt.");
     				ticketpreis = doublearray [3];
     				loop1 = false;
     			  break;
     			case 5:
     				System.out.println("Sie haben eine "+ fahrkartenarray [4] +" gewählt.");
     				ticketpreis = doublearray [4];
     				loop1 = false;
     			  break;
     			case 6:
     				System.out.println("Sie haben eine "+ fahrkartenarray [5] +" gewählt.");
     				ticketpreis = doublearray [5];
     				loop1 = false;
     			  break;
     			case 7:
     				System.out.println("Sie haben eine "+ fahrkartenarray [6] +" gewählt.");
     				ticketpreis = doublearray [6];
     				loop1 = false;
     			  break;
     			case 8:
     				System.out.println("Sie haben eine "+ fahrkartenarray [7] +" gewählt.");
     				ticketpreis = doublearray [7];
     				loop1 = false;
     			  break;
     			case 9:
     				System.out.println("Sie haben eine "+ fahrkartenarray [8] +" gewählt.");
     				ticketpreis = doublearray [8];
     				loop1 = false;
     			  break;
     			case 10:
     				System.out.println("Sie haben eine "+ fahrkartenarray [9] +" gewählt.");
     				ticketpreis = doublearray [9];
     				loop1 = false;
     			  break;
     			 default:
     				 System.out.println("Bitte wählen Sie ein Ticket aus.");
     		  }
             }
            
            while(anzahlDerTickets > 100 || anzahlDerTickets < 1)
            {
            	if(anzahlDerTickets >= 1 || anzahlDerTickets <= 100) 
            	{
            		System.out.print("Anzahl der Tickets: ");
            		anzahlDerTickets = tastatur.nextDouble();
            
            		zuZahlenderBetrag = anzahlDerTickets*ticketpreis;
            		loop2 = false;
            		break;
            	}
            	else
            	{
            		System.out.println("Bitte wählen Sie zwischen 1-10 Tickets.");
            	}
            }
        	return zuZahlenderBetrag;
        }
        
       public static double geldeinwurf(double zuZahlenderBetrag)
       {
       // Geldeinwurf
       // -----------
       Scanner tastatur = new Scanner(System.in);
       double eingezahlterGesamtbetrag = 0.0;
       double eingeworfeneMünze;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.println("\nNoch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag + " Euro"));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       		}
       tastatur.close();
       return eingezahlterGesamtbetrag;
       }
       
       public static void fahrscheinausgabe ()
       // Fahrscheinausgabe
       // -----------------
       {
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("\n\n");
		}
       }
      }

       public static void rueckgeldberechnung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag)
       {
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0 ;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       }


}

