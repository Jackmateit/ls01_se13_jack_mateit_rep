
import java.util.Scanner;
public class arrayEinfuehrung {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
     
    int [] intArray = new int [5] ;
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    
    intArray [2] = 1000;
    intArray [4] = 500 ;
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 
    
    double [] doubleArray = new double [3];
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    
    System.out.println(doubleArray[1]); 
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
   /* 
    int i = 0;
    System.out.println("An welchen Index soll der neue Wert?");
    i = sc.nextInt();
    --i;
    double wert;	
    System.out.println("Geben Sie den neuen Wert f�r index an:");
    wert = sc.nextDouble();
    doubleArray[i] = wert;
    System.out.println(doubleArray[i]);
    
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // 0  0  1000  0  500  
    
    for (int z = 0 ; z < intArray.length; z++) {
    	
    	System.out.println (intArray[z]);
    }
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //1. alle Felder sollen den Wert 0 erhalten
    
    for (int x = 0 ; x < intArray.length; x++) {
    	intArray[x]=0;
    }
    
    //Der intArray soll mit neuen Werten gef�llt werde
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    int v = 1;
    for (int b = 0; b < intArray.length; b++) {
    System.out.println("Geben Sie den Wert f�r die Indexposition "+ v +" ein");
    v++;
    int wert1;
    wert1 = sc.nextInt();
    intArray[b] = wert1;
    }
    System.out.println(intArray[3]);
    */
    //Der intArray soll mit neuen Werten gef�llt werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    int p= 10;
    for (int d = 0; d < intArray.length; d++) {
    intArray[d]= p;
    p += 10;
 
    }
    System.out.println(intArray[0]);
    System.out.println(intArray[2]);
  } // end of main
  
} // end of class arrayEinfuehrung
